<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>庫存表</title>
</head>
<body>
<table border="1">
<tr>
<td colspan="4">目前庫存表</td>
</tr>
<tr>
<td>書本編號</td>
<td>書名</td>
<td>庫存數量</td>
<td>進貨日期</td>
</tr>
<?php
	require_once("dbtools.inc.php");
	$link = create_connection();//建立連線
	
	$sql = "SELECT* FROM 庫存表 Order by 進貨日期 DESC,庫存數量 ASC"; 
	$result = execute_sql($link, "book", $sql);	
	while ($row = mysqli_fetch_array($result)){
		if($row["庫存數量"] > 0){
			echo "<tr>";			
			echo "<td>" . $row['書本編號'] . "</td>";
			echo "<td>" . $row["書名"] . "</td>";
			echo "<td>" . $row["庫存數量"] . "</td>";
			echo "<td>" . $row["進貨日期"] . "</td>";			
			echo "</tr>";
		}else{
			$sql0 = "DELETE FROM 庫存表 WHERE 庫存數量 <= 0";
			$result0 = execute_sql($link, "book", $sql0);			
		}
	}
?>
</table>
<a href = "Purchase.php">進貨紀錄表</a>
<a href = "Shipment.php">出貨紀錄表</a>
</body>
</html>